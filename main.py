import argparse


def to_hex(value) -> str:
    """
    Converts integer value into hex value
    :param value: integer value
    :return: string with hex value
    """
    value = sorted([0, value, 255])[1]
    return '{:0>2}'.format(hex(value)[2:].upper())


def rgb_to_hex(r, g, b) -> str:
    """
    Converts sequence of r,g,b numbers to hex
    :param r, g, b : colors numbers
    :return: string with hex color
    """
    return '#' + to_hex(r) + to_hex(g) + to_hex(b)


def hex_to_rgb(hex_value) -> tuple:
    """
    Convert hex color into tuple of rgb colors
    :param hex_value: hex color in string
    :return: tuple of rgb colors
    """
    rgb = []
    for i in (1, 3, 5):
        decimal = int(hex_value[i:i + 2], 16)
        rgb.append(decimal)
    return tuple(rgb)


def main():
    parser = argparse.ArgumentParser(description='lighten_darken')
    parser.add_argument('-c', '--color', type=str, dest='color',
                        help='input hex color like #FFFFFF')
    parser.add_argument('-p', '--percent', type=int, dest='percent',
                        help='input a percentage lightening or darkening')
    parser.add_argument('-l', '--lighten', dest='lighten', action=argparse.BooleanOptionalAction,
                        help="Write if you wanna light the color")
    parser.add_argument('-d', '--darken', dest='darken', action=argparse.BooleanOptionalAction,
                        help="Write if you wanna dark the color")

    args = parser.parse_args()
    hex_color = args.color
    percentage = args.percent
    get_lighter = args.lighten
    get_darker = args.darken

    if get_lighter:
        colors_tuple = hex_to_rgb(hex_color)
        rgb_colors = [round(colors_tuple[i] + percentage * 2.55) for i in range(3)]
        print(rgb_to_hex(*rgb_colors))
        return
    
    if get_darker:
        colors_tuple = hex_to_rgb(hex_color)
        rgb_colors = [int(colors_tuple[i] - percentage * 2.55) for i in range(3)]
        print(rgb_to_hex(*rgb_colors))


if __name__ == "__main__":
    main()
